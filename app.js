var express = require('express');
var path = require('path');
var cors = require('cors');
const fs = require('fs');

const csvReader = require('csv-reader');
const filePath = path.join(__dirname, '/gladiators.csv');

var whitelist = ['http://localhost:4200']
var corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(null, true)
        }
    }
}

var app = express();

app.use(express.json());

app.use(cors(corsOptions))

app.use(express.static('./public'));

app.get('/public/getGladiators/:year', function (req, res) {
    const year = req.params.year
    let lowerLimit = 1900
    let upperLimit = 2050

    if(year > lowerLimit || year < upperLimit){
        return res.status(404).send("Invalid year")
    }

    console.log("Requesting all American Gladiators that appear on the show during: " + year)

    let gladiatorsToReturn = []
    fs.createReadStream(filePath, 'utf8')
        .pipe(new csvReader({ skipHeader: true, trim: true, allowQuotes:true }))
        .on('data', function (row) {
            let gladiatorName = row[0]
            let realName = row[1]
            let firstYear = parseInt("19" + row[2])
            let lastYear = parseInt("19" + row[3])    

            if (year >= firstYear && year <= lastYear) {
                gladiatorsToReturn.push({
                    'Gladiator Name': gladiatorName,
                    'Real Name': realName,
                    'First Year': firstYear,
                    'Last Year': lastYear
                })
            }
        
        })
        .on('end', function (data) {
            console.log('Successfully read CSV');
            return res.json(gladiatorsToReturn)
        });
})

module.exports = app;
