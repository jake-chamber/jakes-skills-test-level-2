# From Root:
# docker build -t jakechambers/gladiator-party .  
# docker run -p 3000:3000 jakechambers/gladiator-party

FROM node:12

WORKDIR /app
COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm install

COPY . .

CMD [ "node", "bin/www" ]